<?php

namespace App\Http;

use GuzzleHttp\Client;

class MusementAPIRest
{
    private const GET_CITIES_URL = 'https://api.musement.com/api/v3/cities';

    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getCities()
    {
        $res = $this->client->request('GET', self::GET_CITIES_URL);
        return json_decode($res->getBody());
    }

}
