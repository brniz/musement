<?php

namespace App\Http;

use App\Exception\ApiCallException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class HttpClient extends Client
{

    public function request(string $method, $uri = '', array $options = ['http_errors' => false]): ResponseInterface
    {
        $res = parent::request($method, $uri, $options);
        if ($res->getStatusCode() !== 200) {
            throw new ApiCallException($res->getReasonPhrase() . PHP_EOL . $uri . PHP_EOL);
        }
        return $res;
    }

}
