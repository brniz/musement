<?php

namespace App\Http;

use App\Entity\City;

class WeatherAPIRest
{
    private const WEATHERAPI_KEY = "e1ca477fcb3d426da2c235425200912";

    private const GET_FORECAST_URL = "http://api.weatherapi.com/v1/forecast.json?key=%5Byour-key%5D&q=%5Blat%5D,%5Blong%5D&days=2";

    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new HttpClient();
    }

    public function getForecast(City $city)
    {
        $res = $this->httpClient->request('GET', $this->buildUrl(self::GET_FORECAST_URL, ['%5Blat%5D' => $city->getLatitude(), '%5Blong%5D' => $city->getLongitude()]));
        return json_decode($res->getBody());
    }

    private function buildUrl(string $url, array $params = []) : string
    {
        $url = str_replace("%5Byour-key%5D", self::WEATHERAPI_KEY, $url);
        foreach ($params as $key => $value) {
            $url = str_replace($key, $value, $url);
        }
        return $url;
    }

}
