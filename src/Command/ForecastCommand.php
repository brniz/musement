<?php

namespace App\Command;

use App\Exception\ApiCallException;
use App\Service\CityService;

class ForecastCommand
{

    protected $cityService;

    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    public function execute()
    {
        try {
            $cities = $this->cityService->getCities();
            foreach ($cities as $city) {
                $this->cityService->setForecast($city);
                echo 'Processed city ' . $city->getName() . ' | ' . implode(' - ', $city->getForecast()) . PHP_EOL;
            }
        } catch (ApiCallException $ex) {
            echo 'ERROR in API Call: ' . $ex->getMessage() . PHP_EOL;
            return false;
        }
        return true;
    }

}
