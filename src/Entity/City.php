<?php

namespace App\Entity;

class City
{

    private $name;
    private $forecast;
    private $latitude;
    private $longitude;

    public function __construct(string $name, float $latitude, float $longitude)
    {
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->forecast = [];
    }

    function getName(): string
    {
        return $this->name;
    }

    function getForecast(): array
    {
        return $this->forecast;
    }

    function addForecast(string $forecast)
    {
        $this->forecast[] = $forecast;
    }

    function getLatitude(): float
    {
        return $this->latitude;
    }

    function getLongitude(): float
    {
        return $this->longitude;
    }

}
