<?php

namespace App\Exception;

class ApiCallException extends \Exception
{

    protected $message = 'The number of items in the cart has been exceeded';

}
