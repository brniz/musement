<?php

namespace App\Service;

use App\Entity\City;
use App\Http\MusementAPIRest;
use App\Http\WeatherAPIRest;

class CityService
{

    public function getCities(): array
    {
        $cities = [];
        $musementAPI = new MusementAPIRest();
        foreach ($musementAPI->getCities() as $cityAPI) {
            $city = new City($cityAPI->name, $cityAPI->latitude, $cityAPI->longitude);
            $cities[] = $city;
        }
        return $cities;
    }

    /*
     * Note: a forecast object could be used if we want to keep 
     * track of it by days (related to the 2nd step) and 
     */

    public function setForecast(City $city): void
    {
        $weatherAPI = new WeatherAPIRest();
        $forecast = $weatherAPI->getForecast($city);
        foreach ($forecast->forecast->forecastday as $forecastday) {
            $city->addForecast($forecastday->day->condition->text);
        }
    }

}
