# Musement | Backend tech homework

_Musement_

## Information

Name and Surname: **Bernat Magraner**

Company: **Stratesys**

## Proposed solution

### Requirements

Make sure you have installed all of the following prerequisites on your machine:

* PHP7.1-cli - Needed to run the test
* Git - Version control system to download the package
* Composer - Dependency manager to build the project

### Install

Donwload the package

```
git clone https://brniz@bitbucket.org/brniz/musement.git <project-name>
```

Access to project root
```
cd <project-name>
```

Build the project
```
composer install
```

### Step 1 | Development

To check the result you can run:

```
php bin/test-development
```

To run the examples of test units:

```
./vendor/bin/phpunit tests --testdox
```
_I have been experiencing inappropriate behaviours in WeatherAPI. Please check the results of the [url](http://api.weatherapi.com/v1/forecast.json?key=e1ca477fcb3d426da2c235425200912&q=41.396,2.175&days=2 "Barcelona Forecast") executed in the tests in case of getting unexpected results_

### Step 2 | API design

This is the approach of the endpoint to manage Forecasts.

**Notes:**

* Answering questions like Wheather in [city] today/tomorrow, can be done by parsing the date in the call (Ex: date('today')).
* Parameter _date_ in "GET /cities/{cityId}/forecast" is not used as referenced as it is not mandatory in the request.


```
{
    "paths": {
        [...]
        "/cities/{cityId}/forecast": {
            "get": {
                "tags": [
                    "City",
                    "Forecast"
                ],
                "summary": "Get city forecast by unique city identifier",
                "operationId": "GetCitiesCityIdForecast",
                "parameters": [
                    {
                        "$ref": "#/components/parameters/X-Musement-Version"
                    },
                    {
                        "$ref": "#/components/parameters/Accept-Language"
                    },
                    {
                        "$ref": "#/components/parameters/cityId"
                    },
                    {
                        "name": "date",
                        "in": "query",
                        "description": "Filter results by date | Use format: YYYY-MM-DD",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    },
                ],
                "responses": {
                    "200": {
                        "description": "Returned when successful",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/Forecast"
                                    }
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Returned when resource is not found"
                    },
                    "503": {
                        "description": "Returned when the service is unavailable"
                    }
                }
            },
            "put": {
                "tags": [
                    "City",
                    "Forecast"
                ],
                "summary": "Update forecast data to a city. Set to empty not specified forecast for given day",
                "description": "All data must be sent. Empty field will be cleared",
                "operationId": "PutCitiesCityIdForecast",
                "parameters": [
                    {
                        "$ref": "#/components/parameters/X-Musement-Version"
                    },
                    {
                        "$ref": "#/components/parameters/Accept-Language"
                    }
                ],
                "requestBody": {
                    "description": "Forecast put request",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "array",
                                "items": {
                                    "$ref": "#/components/schemas/PutForecast"
                                },
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Returned when successful",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/Forecast"
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Returned on error"
                    },
                    "503": {
                        "description": "Returned when the service is unavailable"
                    }
                },
                "security": [
                    {
                        "content_manager": [
                            "activity:admin"
                        ]
                    }
                ]
            },
            "patch": {
                "tags": [
                    "City",
                    "Forecast"
                ],
                "summary": "Update forecast data to a city",
                "description": "All data must be sent",
                "operationId": "PatchCitiesCityIdForecast",
                "parameters": [
                    {
                        "$ref": "#/components/parameters/X-Musement-Version"
                    },
                    {
                        "$ref": "#/components/parameters/Accept-Language"
                    }
                ],
                "requestBody": {
                    "description": "Forecast patch request",
                    "required": true,
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "array",
                                "items": {
                                    "$ref": "#/components/schemas/PatchForecast"
                                },
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "Returned when successful",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/Forecast"
                                    },
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Returned on error"
                    },
                    "503": {
                        "description": "Returned when the service is unavailable"
                    }
                },
                "security": [
                    {
                        "content_manager": [
                            "activity:admin"
                        ]
                    }
                ]
            },
        },
        [...]
    },
    [...]
    "components": {
        "schemas": {
            [...]
            "City": {
                "properties": {
                    [...]
                    "forecast": {
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Forecast"
                        },
                    },
                },
                [...]
            },
            "Forecast": {
                "properties": {
                    "day": {
                        "title": "Day",
                        "type": "string",
                        "format": "date",
                        "example": "1970-04-13"
                    },
                    "condition": {
                        "title": "Condition",
                        "type": "string"
                    },
                },
                "type": "object",
                "xml": {
                    "name": "Forecast"
                }
            },
            "PutForecast": {
                "required": [
                    "day"
                ],
                "type": "object",
                "allOf": [
                    {
                        "$ref": "#/components/schemas/Forecast"
                    }
                ]
            },
            "PatchForecast": {
                "required": [
                    "day",
                    "condition"
                ],
                "type": "object",
                "allOf": [
                    {
                        "$ref": "#/components/schemas/Forecast"
                    }
                ]
            },
            [..]
        },
        [..]
    },
    [..]
}
```