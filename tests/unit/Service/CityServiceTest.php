<?php

declare(strict_types = 1);

use App\Entity\City;
use App\Exception\ApiCallException;
use App\Service\CityService;
use PHPUnit\Framework\TestCase;

final class CityServiceTest extends TestCase
{

    private $cityService;

    protected function setUp(): void
    {
        $this->cityService = new CityService();
    }

    /**
     * @covers CityService::getCities
     */
    public function testGetCities(): void
    {
        $cities = $this->cityService->getCities();
        $this->assertGreaterThan(0, count($cities));
    }

    /**
     * @covers CityService::setForecast
     */
    public function testSetForecast(): void
    {
        $city = new City('Barcelona', 41.396, 2.175);
        // check to show the previous state
        $this->assertCount(0, $city->getForecast());
        $this->cityService->setForecast($city);
        $this->assertGreaterThan(0, count($city->getForecast()));

        $city = new City('Barcelona', 41000.396, 20000.175);
        $this->expectException(ApiCallException::class);
        $this->cityService->setForecast($city);
    }

}
